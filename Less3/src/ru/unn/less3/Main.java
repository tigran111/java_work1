package ru.unn.less3;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        float arr[] = {4,3, 5, 655, 5445};

        for (int x = 0; x < arr.length ; x++) {
            arr[x] = (float) (arr[x] + (arr[x] * 0.10));
        }

        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    float tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        String arrOur = Arrays.toString(arr);
        System.out.println(arrOur);
    }
}
