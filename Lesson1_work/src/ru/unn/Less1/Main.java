package ru.unn.Less1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //User Name
        System.out.print("Enter your name: ");
        Scanner nam = new Scanner(System.in);
        String name = nam.next();
        System.out.println("Hello " + name);

        // Work 1
        //Two numbers and random math operation
        int First_number, Second_number, result;
        Scanner num = new Scanner(System.in);

        //num1
        System.out.print("Enter first number: ");
        First_number = num.nextInt();

        //num2
        System.out.print("Enter second number: ");
        Second_number = num.nextInt();

        //result
        //change operation
        System.out.println("operation (+) (-) (*) (/)");
        System.out.print("What the operation you want to do: ");
        Scanner oper = new Scanner(System.in);
        String operation = oper.next();

        //option 1
   /*     if (operation.equals("+"))  {
            System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
            result = First_number + Second_number;
            System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
        }
        else if (operation.equals("-")) {
            System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
            result = First_number + Second_number;
            System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
        }
        else if (operation.equals("*")) {
            System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
            result = First_number + Second_number;
            System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
        }
         else if (operation.equals("/")) {
            System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
            result = First_number / Second_number;
            System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
        }
        else {
            System.out.println("What? I Don't understand you. Get to start and try again. Sorry.");
        }*/

        //option2
        switch (operation) {
            case "+":
                System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
                result = First_number + Second_number;
                System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
                break;
            case "-":
                System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
                result = First_number - Second_number;
                System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
                break;
            case "*":
                System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
                result = First_number * Second_number;
                System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
                break;
            case "/":
                System.out.println("Your operation is: " + First_number + " " + operation + " " + Second_number);
                result = First_number / Second_number;
                System.out.println("Your result is: " + result + " Good " + name + ", Very Good.");
                break;
        }

    }
}
