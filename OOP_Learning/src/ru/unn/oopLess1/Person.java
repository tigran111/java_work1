package ru.unn.oopLess1;

public class Person {
    public  int height = 100;
    public String name = "default";

    public void say (String name) {
        System.out.print("Hello " + name);
    }

    public Person() {}

    public Person(int h, String n) {
        height = h;
        name = n;
    }
}
