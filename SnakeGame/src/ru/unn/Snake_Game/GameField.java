package ru.unn.Snake_Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class GameField extends JPanel  implements ActionListener {
    private final int SIZE = 800;
    private final int DOT_SIZE = 20;
    private final int ALL_DOTS = 1200;
    private Image snake;
    private Image apple;
    private int appleX;
    private int appleY;
    private int[] x = new int[ALL_DOTS];
    private int[] y = new int[ALL_DOTS];
    private int dots;
    private Timer timer;
    private boolean left = false;
    private boolean right = true;
    private boolean up = false;
    private boolean down = false;
    private boolean inGame = true;
    private int points;


    public GameField() {
        setBackground(Color.blue);
        loadImages();
        initGame();
        addKeyListener(new FiledKeyListener());
        setFocusable(true);
    }

    public void initGame() {
        dots = 2;
        for (int i = 0; i < dots; i++) {
            y[i] =40 - i* DOT_SIZE;
            x[i] = 100;
        }
        timer = new Timer(250, this);
        timer.start();
        createApple();

    }

    public void createApple() {
        appleX = new Random().nextInt(20)*DOT_SIZE;
        appleY = new Random().nextInt(20)*DOT_SIZE;
    }
    public void loadImages() {
    ImageIcon iia = new ImageIcon("apple.png");
    apple = iia.getImage();

    ImageIcon iid = new ImageIcon("snake.png");
    snake = iid.getImage();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (inGame) {
            g.drawImage(apple, appleX, appleY, this);
            for (int i = 0; i < dots; i++) {
                g.drawImage(snake, y[i], x[i], this);
            }
        }
        else {
            String str  = "Конец игры. Вы собрали " + points + " очков.";
            Font f = new Font("Arial", 15, Font.CENTER_BASELINE);
            g.setColor(Color.RED);
            g.setFont(f);
            g.drawString(str, 125, SIZE/2);
        }
    }

    public void move() {
        for (int i = dots; i > 0 ; i--) {
            x[i] = x[i - 1];
            y[i] = y[i - 1];
        }
        if (left) {
            y[0] -= DOT_SIZE;
        }
        if (right) {
            y[0] += DOT_SIZE;
        }
        if (up) {
            x[0] -= DOT_SIZE;
        }
        if (down) {
            x[0] += DOT_SIZE;
        }
    }
    public void checkApple() {
        if (x[0] == appleY && y[0] == appleX) {
            dots++;
            points++;
            createApple();
        }
    }

    public void checkCollisions() {
        for (int i = dots; i >0 ; i--) {
            if (i>4 && x[0] == x[i] && y[0] == y[i]) {
                inGame = false;
            }

            if (x[0] > 600 - 40) {
                inGame = false;
            }
            if (x[0] < 0) {
                inGame = false;
            }
            if (y[0] > 800 - 60) {
                inGame = false;
            }
            if (y[0] < 0) {
                inGame = false;
            }
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (inGame) {
            checkApple();
            checkCollisions();
            move();
        }
        repaint();
    }

    class FiledKeyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            int key = e.getKeyCode();
            if (key == KeyEvent.VK_LEFT && right == false) {
                left = true;
                up = false;
                down = false;
            }
            if (key == KeyEvent.VK_RIGHT && left == false) {
                right = true;
                up = false;
                down = false;
            }
            if (key == KeyEvent.VK_UP && down == false) {
                up = true;
                left = false;
                right = false;
            }
            if (key == KeyEvent.VK_DOWN && up == false) {
                down = true;
                left = false;
                right = false;
            }
        }
    }
}
