package ru.unn.less4;

import java.util.Random;

public class Circle extends Figure {
    final Random random = new Random();

    public float pi = 3.14f;
    public void square() {
        int radius = random.nextInt(11) - 1;
        float result = pi * (radius * radius);
        System.out.println("Figure: Circle square: " + result);
    }
}
