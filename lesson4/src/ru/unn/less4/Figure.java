package ru.unn.less4;

public abstract class Figure {
    abstract public void square();


    public int getQuadrant(int x, int y) {
        int quadrant = 1;
        if (x > 0 && y > 0)
            quadrant = quadrant;
        else if (x < 0 && y > 0)
            return quadrant += 1;
        else if (x < 0 && y < 0)
            return quadrant += 2;
        else if (x > 0 && y < 0)
            return quadrant += 3;
        return quadrant;
    }
}
