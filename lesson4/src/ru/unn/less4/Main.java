package ru.unn.less4;

import java.lang.reflect.Array;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle();
        Rectangle rectangle = new Rectangle();
        Scanner centerPosition = new Scanner(System.in);

        int x;
        int y;
        Random random = new Random();
        int ran;

        Figure[] figure = new Figure[5];

        for (int i = 0; i < figure.length; i++) {
            ran = random.nextInt(3);

            if (ran == 1) {
                figure[i] = circle;
            }
            else {
                figure[i] = rectangle;
            }

        }

        System.out.print("Do you want to enter the coordinats? [y], [n]: ");
        String enter = centerPosition.nextLine();

        if (enter.equals("y")) {
            for (int i = 0; i < figure.length ; i++) {
                System.out.print("x coordinate: ");
                x = centerPosition.nextInt();
                System.out.print("y coordinate: ");
                y = centerPosition.nextInt();
                System.out.println("coordinate quarter of figure: " + figure[i].getQuadrant(x, y));

                figure[i].square();
            }
        }
        else {
            final int min = -20; // Минимальное число для диапазона
            final int max = 20; // Максимальное число для диапазона
            x = random.nextInt(max - min + 1);
            y = random.nextInt(max - min + 1);

            for (int i = 0; i < figure.length ; i++) {
                figure[i].getQuadrant(x, y);
                figure[i].square();
            }
        }


    }
}
