package ru.unn.less4;

import java.util.Random;

public class Rectangle extends Figure {
    Random random = new Random();


    public void square() {
        int height = random.nextInt(21) - 1;
        int width = random.nextInt(21) - 1;
        int result = height * width;
        System.out.println("Figure: Rectangle square: " + result);
    }
}
